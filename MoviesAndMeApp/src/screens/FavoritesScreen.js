import React from 'react';
import {StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';

import FilmList from '../components/FilmList';
import Avatar from '../components/Avatar';


class FavoritesScreen extends React.Component {

    render() {
        return (
            <View style={styles.main_container}>
                <View style={styles.avatar_container}>
                    <Avatar/>
                </View>
                <FilmList
                    films={this.props.favoritesFilm}
                    navigation={this.props.navigation}
                    favoriteList={true}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
    },
    avatar_container: {
        alignItems: 'center',
    },
});

const mapStateToProps = state => {
    return {
        favoritesFilm: state.toggleFavorite.favoritesFilm,
    };
};

export default connect(mapStateToProps)(FavoritesScreen);
